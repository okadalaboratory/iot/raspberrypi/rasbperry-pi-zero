# Raspberry Pi Zero 3.5インチディスプレイ

- Raspberry Pi Zero WH / Zero W/Zero W 2用最速3.5インチタッチスクリーン、Super HD 480x320解像度、60+ fps
- Pi Zero WH / Zero w/ Zero W 2、ポータブルモニターに完璧にフィット。
- 3.5インチの高速Raspberry Piタッチスクリーン用のカスタマイズ回路基板。
- **注意:Raspberry Pi Zero and Zero W/Zero W 2にのみ適合します。**


<img src="../images/61m91HYmgcL._AC_SL1500_ (1).jpg" width=512>

<img src="../images/71BuMEZs3nL._AC_SL1500_ (1).jpg" width=512>


[Git](https://github.com/iUniker/Pi-Zero-3.5-Inch-Screen)




