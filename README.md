# Rasbperry Pi Zero

<img src="images/011.jpg" width=512>

- Raspberry Pi Zero W

- Raspberry Pi Zero WH

- Raspberry Pi Zero 2W


## 仕様
Raspberry Pi Zero WH

- Broadcom BCM2835
- CPU 1000MHz シングルコア ARM1176JZ-F（ARMv6）
- GPU デュアルコア VideoCore IV〓 250MHz
- OpenGL ES 2.0対応、ハードウェアOpenVG対応、1080p30 H.264 high-profileデコード
- 1Gピクセル/秒、1.5Gテクセル/秒、または24G FLOPの性能を持つ
- メモリー 512MB DDR2 低電圧 SDRAM
- WiFi, Bluetooth
- SPI、I2C、UART、PWM
- 3,000円程度


[Raspberry Pi zero データシート - raspberrypi.com]()



## ピンレイアウト
<img src="images/20210427-1.jpg" width=512>


## Raspberry Pi OSのインストール
電源を入れるだけでWiFi経由でリモートログインできる状態にする。

　1. 準備するもの
- 8GB以上のMicro SDカード。<br>
[高耐久なもの](https://www.westerndigital.com/ja-jp/products/memory-cards/sandisk-max-endurance-uhs-i-microsd?sku=SDSQQVR-032G-JN3ID)

- 色々な情報
    - ホスト名
    - SSHで接続する際のパスワード
    - WiFiで仕様するSSIDとパスワード
    - タイムゾーン
    - キーボードレイアウト

2. Raspberry Pi OS Imagerのダウンロード
[公式サイト](https://www.raspberrypi.com/software/)からインストール用ソフトをダウンロードする。

3. Raspberry Pi OSイメージの設定
    1. Raspberry Pi OS Imagerの起動
    2. Raspberry Pi OS (32-bit)を選択
    3. 書き込みストレージの選択
    4. Raspberry Pi OSの事前設定<br>
    メインメニュー画面で、CTRL + SHIFT + x を押すと設定画面が現れます。下記の情報を入力してください。<br>
        - ホス名
        - SSHで接続する際のパスワード
        - WiFiで仕様するSSIDとパスワード
        - タイムゾーン
        - キーボードレイアウト

4. Raspberry Pi OSイメージの書き込み


5. (オプション) USB経由でのインターネット接続を有効にする
6. (オプション)Bluetoothテザリングによるネットワーク設定
mac OS Montereyから、Bluetoothによるテザリングは行えない


## 初めての起動
### ディスプレイとキーボード、マウスを接続する

### 他のPCからSSHでアクセスする
```
ssh ホスト名.local
```
```
$ sudo apt-get update
$ sudo apt-get upgrade
```




## e-Paper
https://www.waveshare.com/e-paper-driver-hat.htm



## Ubuntuのインストール


## リンク
[オフィシャルサイト](http://www.raspberrypi.org/)

[Raspberry Pi Zero W（Raspberry Pi Zero Wireless）](https://www.raspberrypi.org/products/raspberry-pi-zero-w/)

[Raspberry Pi Zero(W, WH, 2 W)のセットアップ](https://qiita.com/hishi/items/8bdfd9d72fa8fe2e7573)